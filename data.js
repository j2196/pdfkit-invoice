const data = [
    {
        id: 1,
        shipping: {
          name: "Sanith Garipally",
          address: "8-101",
          city: "Kamareddy",
          state: "Telangana",
          country: "India",
          postal_code: 503111
        },
        items: [
          {
            item: "Chicken Lollipop",
            quantity: 2,
            amount: 120
          },
          {
            item: "Hyderabadi Chicken Dum Biryani",
            quantity: 1,
            amount: 150
          }
        ],
        subtotal: 270,
        packingCharges: 40,
        deliveryCharges: 10
    },
    {
        id: 2,
        shipping: {
          name: "Ajay",
          address: "5-3-565/b",
          city: "Kamareddy",
          state: "Telangana",
          country: "India",
          postal_code: 503111
        },
        items: [
          {
            item: "Chicken Fried Rice",
            quantity: 1,
            amount: 100
          },
        ],
        subtotal: 100,
        packingCharges: 0,
        deliveryCharges: 40
    },
    {
        id: 3,
        shipping: {
          name: "Nandy",
          address: "1-121/b",
          city: "Thirunalvelli",
          state: "Tamilnadu",
          country: "India",
          postal_code: 627401
        },
        items: [
          {
            item: "Mutton Kheema Biryani",
            quantity: 1,
            amount: 200
          },
          {
            item: "Strawberry Milkshake",
            quantity: 1,
            amount: 60
          },
        ],
        subtotal: 260,
        packingCharges: 0,
        deliveryCharges: 0
    },
    {
        id: 4,
        shipping: {
          name: "Srikanth",
          address: "122-1/b",
          city: "Eluru",
          state: "Andhrapradesh",
          country: "India",
          postal_code: 534001
        },
        items: [
          {
            item: "Prawns Biryani",
            quantity: 1,
            amount: 260
          },
        ],
        subtotal: 260,
        packingCharges: 0,
        deliveryCharges: 0
    },
    {
        id: 5,
        shipping: {
          name: "Sanith",
          address: "5-3-565/b",
          city: "Kamaredy",
          state: "Telangana",
          country: "India",
          postal_code: 503111
        },
        items: [
          {
            item: "Chicken Dum Biryani",
            quantity: 1,
            amount: 150
          },
          {
            item: "Butter Naan",
            quantity: 2,
            amount: 50
          },
          {
            item: "Chicken Muglai",
            quantity: 1,
            amount: 180
          },
        ],
        subtotal: 380,
        packingCharges: 0,
        deliveryCharges: 0
    },
    {
        id: 6,
        shipping: {
          name: "Nikitha Garipally",
          address: "8-101",
          city: "Kamareddy",
          state: "Telangana",
          country: "India",
          postal_code: 503111
        },
        items: [
          {
            item: "Mushroom Biryani",
            quantity: 1,
            amount: 120
          }
        ],
        subtotal: 120,
        packingCharges: 40,
        deliveryCharges: 10
    },
    {
        id: 7,
        shipping: {
          name: "Ajay Kodipyaka",
          address: "5-3-565/b",
          city: "Kamareddy",
          state: "Telangana",
          country: "India",
          postal_code: 503111
        },
        items: [
          {
            item: "Chicken Dum Biryani",
            quantity: 1,
            amount: 100
          },
          {
            item: "Coke",
            quantity: 1,
            amount: 80
          },

        ],
        subtotal: 180,
        packingCharges: 0,
        deliveryCharges: 40
    },
    {
        id: 8,
        shipping: {
          name: "Nandy",
          address: "1-121/b",
          city: "Thirunalvelli",
          state: "Tamilnadu",
          country: "India",
          postal_code: 627401
        },
        items: [
          {
            item: "Prawns Pepper Fry",
            quantity: 1,
            amount: 180
          },
          {
            item: "Chicken Lollipop Biryani",
            quantity: 1,
            amount: 200
          },
          {
            item: "Arabian Grape Juice",
            quantity: 1,
            amount: 60
          },
        ],
        subtotal: 440,
        packingCharges: 0,
        deliveryCharges: 0
    },
    {
        id: 9,
        shipping: {
          name: "Srikanth Sri",
          address: "12-1/b",
          city: "Eluru",
          state: "Andhrapradesh",
          country: "India",
          postal_code: 534001
        },
        items: [
          {
            item: "Apollo Fish Fry",
            quantity: 1,
            amount: 380
          },
        ],
        subtotal: 380,
        packingCharges: 0,
        deliveryCharges: 0
    },
    {
        id: 10,
        shipping: {
          name: "Krishnamurthy",
          address: "5-3-565/b",
          city: "Kamaredy",
          state: "Telangana",
          country: "India",
          postal_code: 503111
        },
        items: [
          {
            item: "Veg Pulav",
            quantity: 1,
            amount: 150
          },
          {
            item: "Gulab Jamoon",
            quantity: 2,
            amount: 50
          },
        ],
        subtotal: 250,
        packingCharges: 0,
        deliveryCharges: 0
    },
    
]


module.exports = data;