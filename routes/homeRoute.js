const express = require('express');
const router = express.Router();
const {homeController,orderController,downloadInvoiceController} = require("../controllers/homeController")

router.get("/", homeController);
router.post("/getOrder", orderController);
router.post("/downloadInvoice", downloadInvoiceController)

module.exports = router;
