const express = require("express");
const app = express();
const PORT = process.env.port || 5000

const homeRoute = require("./routes/homeRoute")

app.set('view engine', 'ejs')
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/", homeRoute)

app.listen(PORT, () => console.log(`Server running on Port ${PORT}`))