const data = require("../data")
const path = require('path')
const { createInvoice } = require("../createInvoice");

const homeController = (req, res) => {
    res.render("pages/index")             
};

const orderController = (req, res) => {
    let requiredData = data.filter((object) => object.id == req.body.id)
    if(requiredData.length) {
        //creating pdf after matching user entered id with one an id from data
        createInvoice(requiredData[0], "invoice.pdf");
        res.render("pages/displayOrder", {
            data : requiredData[0]
        })
    } else {
        res.render("pages/idErrorPage", {
            id: req.body.id
        })
    }
}

const downloadInvoiceController = (req, res) => {
    res.download(path.join(__dirname, "../invoice.pdf"))
}

module.exports = {
    homeController,
    orderController,
    downloadInvoiceController
}