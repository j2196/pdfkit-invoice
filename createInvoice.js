const fs = require("fs");
const PDFDocument = require("pdfkit");

function createInvoice(invoice, path) {
  let doc = new PDFDocument({ size: "A4", margin: 50 });

  generateHeader(doc);
  generateCustomerInformation(doc, invoice);
  generateInvoiceTable(doc, invoice);
  generateFooter(doc);

  doc.end();
  doc.pipe(fs.createWriteStream(path));
}

function generateHeader(doc) {
  doc
    .image("logo.png", 450 , 50, { 
        width: 100,
    })
    .moveDown();
}

function generateCustomerInformation(doc, invoice) {
  doc
    .fillColor("#444444")
    .fontSize(12)
    .text(`Thanks for choosing Swiggy, ${invoice.shipping.name}! Here are your order details:`, 40, 110);

  const customerInformationTop = 150;

  doc
    .fontSize(12)
    .font("Helvetica")
    .text("Invoice No:", 42, customerInformationTop)
    .font("Helvetica-Bold")
    .text(`#${invoice.id}`, 104, customerInformationTop)
    .font("Helvetica")
    .text("Order placed at:", 42, customerInformationTop + 25)
    .font("Helvetica-Bold")
    .text(formatDate(new Date()), 132, customerInformationTop + 25)
    .font("Helvetica")
    .text("Order delivered at:", 42, customerInformationTop + 50)
    .font("Helvetica-Bold")
    .text(formatDate(new Date()), 145, customerInformationTop + 50)
    .font("Helvetica")
    .text("Order Status:", 42, customerInformationTop + 75)
    .font("Helvetica-Bold")
    .text("Delivered", 116, customerInformationTop + 75)

    .font("Helvetica")
    .text('Delivery To:', 400, customerInformationTop)
    .font("Helvetica-Bold")
    .text(invoice.shipping.name, 400, customerInformationTop + 25)
    .font("Helvetica")
    .text(invoice.shipping.address, 400, customerInformationTop + 50)
    .text(
      invoice.shipping.city +
        ", " +
        invoice.shipping.state +
        ", " +
        invoice.shipping.country + ", " + invoice.shipping.postal_code,
      400,
      customerInformationTop + 75
    )

    .font("Helvetica")
    .text('Ordered from:', 42, customerInformationTop + 120)
    .font("Helvetica-Bold")
    .text("Biryani Kitchen", 42, customerInformationTop + 145)
    .font('Helvetica')
    .text("26/2, Opposite Bangalore Central Mall, Near Eco space IT Park", 42, customerInformationTop + 170)
    .text("Outer Ring Road, Bellundur, Bangalore-560103", 42, customerInformationTop + 190)
    
    .moveDown();
}

function generateInvoiceTable(doc, invoice) {
  let i;
  const invoiceTableTop = 380;

  doc.font("Helvetica-Bold");
  generateTableRow(
    doc,
    invoiceTableTop,
    "Item Name",
    "Quantity",
    "Price"
  );
  generateHr(doc, invoiceTableTop + 20);
  doc.font("Helvetica");

  for (i = 0; i < invoice.items.length; i++) {
    const item = invoice.items[i];
    const position = invoiceTableTop + (i + 1) * 30;
    generateTableRow(
      doc,
      position,
      item.item,
      item.quantity,
      formatCurrency(item.amount)
    );

    generateHr(doc, position + 20);
  }

  const subtotalPosition = invoiceTableTop + (i + 1) * 30;
  generateTableRow(
    doc,
    subtotalPosition,
    "Item Total:",
    "",
    formatCurrency(invoice.subtotal)
  );

  const gstPosition = subtotalPosition + 20;
    generateTableRow(
        doc,
        gstPosition,
        "GST:",
        "",
        formatGST(invoice.subtotal)
    )

  const orderPackingPosition = gstPosition + 20;
  generateTableRow(
    doc,
    orderPackingPosition,
    "Order Packing Charges",
    "",
    "Rs " + (invoice.packingCharges).toFixed(2)
  );

  const deliveryChargesPosition = orderPackingPosition + 20;
  generateTableRow(
    doc,
    deliveryChargesPosition,
    "Delivery Charges",
    "",
    "Rs " + (invoice.deliveryCharges).toFixed(2)
  );
  doc.font("Helvetica");
  generateHr(doc, deliveryChargesPosition + 20)

  const totalPosition = deliveryChargesPosition + 40;
  generateTableRow(
    doc,
    totalPosition,
    "TOTAL",
    "",
    formatTotal(invoice.subtotal, invoice.packingCharges, invoice.deliveryCharges)
  );
}

function generateFooter(doc) {
  doc
    .fontSize(12)
    .font("Helvetica-Bold")
    .text("Disclaimer: ", 42,730)
    .fontSize(10)
    .font("Helvetica")
    .text(
      "This is an acknowledgement of Delivery of the Order and not an actual invoice. Details mentioned above including the menu prices and taxes (as applicable) are as provided by the Restaurant to Swiggy. Responsibility of charging (or not charging) taxes lies with the Restaurant and Swiggy disclaims any liability that may arise in this respect."
    );
}

function generateTableRow(
  doc,
  y,
  item,
  quantity,
  lineTotal
) {
  doc
    .fontSize(12)
    .text(item, 50, y)
    .text(quantity, 280, y, {width: 50, align: "center" })
    .text(lineTotal, 0, y, { align: "right" });
}

function generateHr(doc, y) {
  doc
    .strokeColor("#aaaaaa")
    .lineWidth(1)
    .moveTo(50, y)
    .lineTo(550, y)
    .stroke();
}

function formatCurrency(cents) {
  return "Rs " + (cents).toFixed(2);
}

function formatGST(rupees) {
    return "Rs " + (rupees * 5 / 100).toFixed(2)
}

function formatTotal(subtotal, packingCharge, deliveryCharge) {
    return "Rs " + (subtotal + (subtotal * 5 / 100) + packingCharge + deliveryCharge).toFixed(2)
}

function formatDate(date) {
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();

  return year + "/" + month + "/" + day;
}

module.exports = {
  createInvoice
};